# Big Data Analyse - Code Competition 2018-12

_Lösung zur [IT-Talents Code Competition 2018-12](https://www.it-talents.de/foerderung/code-competition/code-competition-12-2018)_

## Überblick

Diese Lösung analysiert den [Datensatz eines Rennspiels](races.csv) mit Hilfe von Python Tools.
Außerdem beinhaltet sie ein einfaches Machine-Learning Modell zur Vorhersage von weiteren Rennergebnissen.
Das Projekt besteht aus:

* Einem [Jupyter-Notebook](Datenanalyse.ipynb), das Analyse, Visualisierung und Erstellung des Vorhersagemodells beinhaltet.  
    Das Notebook nutzt einige kleine Hilfsmethoden aus dem Modul `races_data_helpers.py`.
* Im Download-Archiv: Das vortrainierte Vorhersagemodell in `model/race_classifier.pkl` sowie 
    einen Export des Jupyter-Notebooks als [HTML-Website](Datenanalyse.html).  
    (Achtung: Diese Dateien sind _nicht_ im Git-Repository enthalten!)
* Ein kleines Skript (`predict.py`), über das das vortrainierte Modell genutzt werden kann.
    Das Skript liefert Vorhersagen zu Eingaben im JSON-Format wie in [test-data.json](test-data.json).

## Setup

### Voraussetzungen
* Python 3 (https://www.python.org/downloads/) mit pip
* Empfohlen: virtualenv zum Erstellen einer virtuellen Umgebung für das Projekt  
    (virtualenv kann mit pip über `pip3 install virtualenv`, bzw. mit `--user` Option für Installation nur für den Benutzer, installiert werden)

### Installationsschritte
1. Herunterladen des Projektarchivs von https://bitbucket.org/quanik/codecompetition-2018-12/downloads/.
2. (Falls mit virtualenv) Erstellen einer neuen virtuellen Umgebung im Projektordner:  
    ```
    virtualenv env
    ```
    Anschließend kann die virtuelle Umgebung mit `source ./env/bin/activate` (auf Linux) bzw. `.\env\Scripts\activate` (auf Windows) aktiviert werden.
3. Zum Ausführen des Notebooks und der Skripte sind einige Python Libraries nötig (z.B. jupyter, pandas und scikit-learn).  
    Diese können innerhalb des Projektordners einfach über pip installiert werden:  
    ```
    pip3 install -r requirements.txt
    ```
4. Hat alles geklappt, kann das Jupyter-Notebook über `jupyter notebook` gestartet und angezeigt werden (Datei `Datenanalyse.ipynb`).
