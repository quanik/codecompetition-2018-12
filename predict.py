# 
# Ein kleines Skript, dass das vortrainierte scikit-learn Modell verwendet,
# um die Ausgänge von Rennen vorherzusagen.
# Übergib den Namen einer Json-Datei mit Eingabewerten, z.B. `python predict.py test-data.json`
# (test-data.json enthält eine beispielhafte Eingabe.)
# 
import sys
import json
import pandas as pd
from sklearn.externals import joblib

pre_transform, clf = joblib.load("model/race_classifier.pkl")

def predict_proba(input_data):
    input_df = pd.DataFrame(input_data)
    model_input = pre_transform.transform(input_df)
    return clf.predict_proba(model_input)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Error: Please specify a file with input data as first parameter.")
        exit()
    with open(sys.argv[1], 'r') as f:
        data = json.load(f)
    predictions = predict_proba(data)
    results = []
    for in_data, pred in zip(data, predictions):
        if pred[1] > pred[0]:
            winner = in_data['challenger']
        else:
            winner = in_data['opponent']
        results.append({'winner': winner, 'probability': max(pred)})
    print(json.dumps(results, indent=2))
