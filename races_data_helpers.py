import pandas as pd
import numpy as np
import phpserialize
from scipy import stats
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score
from sklearn.metrics import roc_curve

DATA_FILE="races.csv"

def load_and_prepare_races(file=DATA_FILE):
    '''Load the race data from file and set the right data types.
    
    :param file: the CSV file containing the data, defaults to DATA_FILE
    :return: a pandas data frame
    '''
    races = pd.read_csv(file, sep=';', index_col=0)
    # adjust some data types
    races["race_created"] = pd.to_datetime(races["race_created"], format="%d.%m.%Y")
    races["race_driven"] = pd.to_datetime(races["race_driven"], errors="coerce")
    races["fuel_consumption"] = pd.to_numeric(races["fuel_consumption"], errors="coerce")
    # set type of categorical data
    races["status"] = races["status"].astype("category")
    races["weather"] = races["weather"].astype("category")
    races["track_id"] = races["track_id"].astype("category")
    # decode weather forecast
    races["forecast"] = [phpserialize.loads(str.encode(x), decode_strings=True) for x in races["forecast"]]
    return races

def remove_unfinished(df):
    '''Remove all unfinished races
    
    :return: a dataframe with the changes applied
    '''
    new_df = df[df["status"] == "finished"]
    new_df = new_df.drop("status", axis=1)
    return new_df

def add_winner_bool(df):
    '''Add a boolean attribute indicating whether the challenger has won.
    
    :return: a dataframe with the changes applied
    '''
    df["challenger_won"] = df["challenger"].astype(int) == df["winner"].astype(int)
    return df

def add_forecast_correctness(df):
    '''Add a float value showing how correct the weather forecast was.
    
    :return: a dataframe with the changes applied
    '''
    df["forecast_correctness"] = np.array([x["forecast"][x["weather"]]/100.0 for i, x in df.iterrows()])
    return df

def plot_challenger_won(ax, df, title="Wie oft hat der Herausforderer gewonnen?"):
    '''Plot a pie chart showing where the challenger has won.
    
    :param ax: the matplotlib axes on which to plot
    :param df: the pandas data frame containing the items
    :param title: a title, defaults to "Wie oft hat der Herausforderer gewonnen?"
    '''
    df_wins = df.groupby('challenger_won').size().rename("")
    df_wins.plot.pie(ax=ax, title=title, startangle=90,
            autopct="%.1f%%", explode=[0.03]*2)

def get_race_counts(df):
    '''Count the number of races for each driver.

    :return: a data frame listing the number of races for each driver
    '''

    challenge_counts = df.groupby("challenger").size()
    opponent_counts = df.groupby("opponent").size()
    race_counts = challenge_counts.add(opponent_counts, fill_value=0)
    return race_counts

def print_stat_values(values, text="Statistische Werte"):
    '''Print some statistical metrics for a value set.
    
    :param values: the given values
    :param text: a title, defaults to "Statistische Werte"
    '''
    print("--- {} ---".format(text))
    print("Anzahl: ", len(values))
    print("Minimum: ", values.min())
    print("Maximum: ", values.max())
    print("Durchschnitt: {:.2f}".format(values.mean()))
    print("Median: {:.2f}".format(values.median()))
    print("Modus: {:.2f}".format(stats.mode(values).mode[0]))

def split_experiences(df):
    '''Splits the given data frame by 'exp_ratio'.
    '''
    df_s = df[df['exp_ratio'] < 1].groupby('challenger_won').size()
    df_e = df[df['exp_ratio'] == 1].groupby('challenger_won').size()
    df_g = df[df['exp_ratio'] > 1].groupby('challenger_won').size()
    return pd.concat([df_s, df_e, df_g], axis=1)

def print_metrics(y_true, y_predict, name="Results"):
    '''Calculate and print some statistical metrics for a classification model.
    
    :param y_true: the true labels
    :param y_predict: the predicted labels
    :param name: a title, defaults to "Results"
    '''
    print("--- {} ---".format(name))
    print("Accuracy:  ", accuracy_score(y_true, y_predict))
    print("Precision: ", precision_score(y_true, y_predict))
    print("Recall:    ", recall_score(y_true, y_predict))
    print("Confusion matrix: ")
    print(confusion_matrix(y_true, y_predict))
    
def plot_confusion_matrix(ax, name, y_true, y_predict, labels=["Nein", "Ja"]):
    '''Visualize a confusion matrix.
    
    :param ax: The matplotlib axes on which to draw the matrix
    :param name: The title of the plot
    :param y_true: The true labels
    :param y_predict: The predicted labels
    :param labels: The tick labels, defaults to ["Nein", "Ja"]
    '''
    mat = confusion_matrix(y_true, y_predict)
    im = ax.imshow(mat, cmap=plt.cm.Blues, vmin=0, vmax=mat.sum()/2)
    ax.set_xticks(range(mat.shape[0]))
    ax.set_xticklabels(labels)
    ax.set_yticks(range(mat.shape[1]))
    ax.set_yticklabels(labels)
    ax.set_xlabel("Vorhergesagt")
    ax.set_ylabel("Tatsächlich")
    ax.set_title(name)
    for x in range(mat.shape[0]):
        for y in range(mat.shape[1]):
            ax.text(x, y, mat[x,y], ha="center")
    return im

def plot_roc_curve(ax, clf, y, X, style='-', label=None):
    '''Plot a ROC curve for the given classifier.
    
    :param ax: the matplotlib axes on which to plot
    :param clf: the classifier to be used
    :param y: the target labels
    :param X: the input values
    :param style: the matplotlib line style, defaults to '-'
    :param label: the label of the plot, defaults to None
    '''
    probas = clf.predict_proba(X)
    fpr, tpr, thres = roc_curve(y, probas[:, 1])
    ax.axis([0, 1, 0, 1])
    ax.set_xlabel('FPR')
    ax.set_ylabel('TPR')
    return ax.plot(fpr, tpr, style, label=label)
