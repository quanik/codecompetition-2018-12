jupyter==1.0.0
matplotlib==3.0.2
numpy==1.15.4
pandas==0.23.4
phpserialize==1.3
scikit-learn==0.20.2
scipy==1.2.0
